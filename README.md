# Technocarte

La Technocarte qui t'affiche la Technopolice !

## Installation

Pour utiliser cette carte, il suffit de mettre la page `index.html`, ainsi que les fichiers javascript, sur un serveur web.

Par exemple, pour faire vos tests, vous pouvez vous rendre dans ce dossier depuis votre terminal, et lancer la commande `python -m http.server`. Rendez-vous ensuite sur le lien qui s'affiche ( du style `Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...`), et vous verrez la carte s'afficher.

## Mettre à jour données

On lance `make` et tout se met à jour.  
Vous aurez besoin de l'outil `yq` [trouvable ici](https://github.com/mikefarah/yq).

### France

#### Villes

Chaque ville est représentée par son code INSEE dans `data/fr/cities/<code>.yml`. Le script `genFrCitiesData.sh` génère le fichier de données à partir de ces fichiers.
Un outil web pour récupérer ces codes INSEE des villes se trouve ici : https://geo.api.gouv.fr/decoupage-administratif/communes#name

#### Départements

Chaque département est représenté par son code INSEE dans `data/fr/departements/<code>.yml`. Le script `genFrDepartementsData.sh` génère le fichier de données à partir de ces fichiers.

#### Régions

Chaque région est représentée par son code INSEE dans `data/fr/regions/<code>.yml`. Le script `genFrRegionsData.sh` génère le fichier de données à partir de ces fichiers.

### Belgique

#### Villes

Chaque ville est représentée par son code NSI dans `data/be/cities/<code>.yml`. Le script `genBeCitiesData.sh` génère le fichier de données à partir de ces fichiers.
