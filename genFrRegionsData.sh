#!/usr/bin/env bash

set -e

declare -A types_list=()

mkdir -p "./.cache/fr/regions/geo_api_gouv_fr/"
mkdir -p "./.cache/fr/regions/france_geojson/"

echo -n "" > geo-data-regions-fr.js

for file in ./data/fr/regions/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	code=$(basename "$file" .yml)
	description=$(cat $file | yq -r .fr.description)
	description=$(sed 's/"/\\"/g' <<<$description)
	url=$(cat $file | yq -r .fr.url)
	if [ ! -f "./.cache/fr/regions/geo_api_gouv_fr/${code}.txt" ]; then
		cache=$(curl --silent "https://geo.api.gouv.fr/regions?code=${code}")
		echo "${cache}" > "./.cache/fr/regions/geo_api_gouv_fr/${code}.txt"
	else
		cache=$(cat "./.cache/fr/regions/geo_api_gouv_fr/${code}.txt")
	fi
	nom=$(echo ${cache} | jq -r .[0].nom)
	id=$(echo $nom | awk '{print tolower($0)}' | tr "' " "--")
	id=$(echo $id | iconv -f utf8 -t ascii//TRANSLIT)

	if [ ! -f "./.cache/fr/regions/france_geojson/${code}.txt" ]; then
		cache_geo=$(curl --silent "https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/regions/${id}/region-${id}.geojson" | jq -c .geometry)
		echo "${cache_geo}" > "./.cache/fr/regions/france_geojson/${code}.txt"
	else
		cache_geo=$(cat "./.cache/fr/regions/france_geojson/${code}.txt")
	fi

	categories=""
	for type in $(cat $file | yq -c -r '.meta.types[]'); do
		types="${types} ${type}"
		types_list[$type]="region_fr_${code},${types_list[$type]}"
		if [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta') != 'null' ]] && [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]') != 'null' ]]; then
			for depends_on in $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]'); do
				types="${types} ${depends_on}"
				types_list[$depends_on]="region_fr_${code},${types_list[$depends_on]}"
			done
		fi
		categories="\"${type}\",${categories}"
	done

	if [ "$url" == "null" ]; then
		echo "var region_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\", \"categories\": [${categories}]},\"geometry\":$cache_geo}, regionsProperties);" >> geo-data-regions-fr.js
	else
		echo "var region_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\", \"categories\": [${categories}],\"url\":\"$url\"},\"geometry\":$cache_geo}, regionsProperties);" >> geo-data-regions-fr.js
	fi
	ids="region_fr_${code},${ids}"
done

echo "var typeRegionsFr = {" >> geo-data-regions-fr.js
for type in $(echo $types | xargs -n1 | sort -u | xargs); do
	echo "  ${type}: [${types_list[$type]}]," >> geo-data-regions-fr.js
done
echo "};" >> geo-data-regions-fr.js

echo "var geoDataRegionsFr = [${ids}];" >> geo-data-regions-fr.js