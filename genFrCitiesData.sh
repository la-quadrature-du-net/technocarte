#!/usr/bin/env bash

set -e

declare -A types_list=()

mkdir -p "./.cache/fr/cities/"

echo -n "" > ./geo-data-cities-fr.js

for file in ./data/fr/cities/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	code=$(basename "$file" .yml)
	description=$(cat $file | yq -r .fr.description)
	description=$(sed 's/"/\\"/g' <<<$description)
	url=$(cat $file | yq -r .fr.url)
	if [ ! -f "./.cache/fr/cities/${code}.txt" ]; then
		cache=$(curl --silent "https://geo.api.gouv.fr/communes?code=${code}&fields=name,centre,contour")
		echo "${cache}" > "./.cache/fr/cities/${code}.txt"
	else
		cache=$(cat "./.cache/fr/cities/${code}.txt")
	fi
	nom=$(echo ${cache} | jq -c -r .[0].nom)
	lng=$(echo ${cache} | jq .[0].centre.coordinates[0])
	lat=$(echo ${cache} | jq .[0].centre.coordinates[1])
	geo=$(echo ${cache} | jq -c .[0].contour)

	categories=""
	for type in $(cat $file | yq -c -r '.meta.types[]'); do
		types="${types} ${type}"
		types_list[$type]="city_fr_${code},${types_list[$type]}"
		if [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta') != 'null' ]] && [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]') != 'null' ]]; then
			for depends_on in $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]'); do
				types="${types} ${depends_on}"
				types_list[$depends_on]="city_fr_${code},${types_list[$depends_on]}"
			done
		fi
		categories="\"${type}\",${categories}"
	done

	if [ "$url" == "null" ]; then
		echo "var city_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":[$lat,$lng], \"categories\": [${categories}]},\"geometry\":$geo}, citiesProperties);" >> ./geo-data-cities-fr.js
	else
		echo "var city_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":[$lat,$lng], \"categories\": [${categories}],\"url\":\"$url\"},\"geometry\":$geo}, citiesProperties);" >> ./geo-data-cities-fr.js
	fi
	ids="city_fr_${code},${ids}"

done

echo "var typeCitiesFr = {" >> geo-data-cities-fr.js
for type in $(echo $types | xargs -n1 | sort -u | xargs); do
	echo "  ${type}: [${types_list[$type]}]," >> geo-data-cities-fr.js
done
echo "};" >> geo-data-cities-fr.js

echo "var geoDataCitiesFr = [${ids}];" >> geo-data-cities-fr.js
