#!/usr/bin/env bash

set -e

declare -A types_list=()

mkdir -p "./.cache/be/communes/"

echo -n "" > geo-data-cities-be.js

for file in ./data/be/cities/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	code=$(basename "$file" .yml)
	description=$(cat $file | yq -r .fr.description)
	description=$(sed 's/"/\\"/g' <<<$description)
	url=$(cat $file | yq -r .fr.url)
	if [ ! -f "./.cache/be/communes/${code}.txt" ]; then
		cache=$(curl --silent "https://data.namur.be/api/records/1.0/search/?dataset=communes-belges&q=nsi%3D${code}")
		echo "${cache}" > "./.cache/be/communes/${code}.txt"
	else
		cache=$(cat "./.cache/be/communes/${code}.txt")
	fi
	nom=$(echo ${cache} | jq -c -r .records[0].fields.name)
	marker=$(echo ${cache} | jq -c .records[0].fields.geo_point_2d)
	geo=$(echo ${cache} | jq -c .records[0].fields.geo_shape)

	categories=""
	for type in $(cat $file | yq -c -r '.meta.types[]'); do
		types="${types} ${type}"
		types_list[$type]="city_be_${code},${types_list[$type]}"
		if [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta') != 'null' ]] && [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]') != 'null' ]]; then
			for depends_on in $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]'); do
				types="${types} ${depends_on}"
				types_list[$depends_on]="city_be_${code},${types_list[$depends_on]}"
			done
		fi
		categories="\"${type}\",${categories}"
	done

	if [ "$url" == "null" ]; then
		echo "var city_be_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":$marker, \"categories\": [${categories}]},\"geometry\":$geo}, citiesProperties);" >> geo-data-cities-be.js
	else
		echo "var city_be_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":$marker, \"categories\": [${categories}],\"url\":\"$url\"},\"geometry\":$geo}, citiesProperties);" >> geo-data-cities-be.js
	fi
	ids="city_be_${code},${ids}"
done

echo "var typeCitiesBe = {" >> geo-data-cities-be.js
for type in $(echo $types | xargs -n1 | sort -u | xargs); do
	echo "  ${type}: [${types_list[$type]}]," >> geo-data-cities-be.js
done
echo "};" >> geo-data-cities-be.js

echo "var geoDataCitiesBe = [${ids}];" >> geo-data-cities-be.js
