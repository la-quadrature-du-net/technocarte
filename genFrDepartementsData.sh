#!/usr/bin/env bash

set -e

declare -A types_list=()

mkdir -p "./.cache/fr/departements/geo_api_gouv_fr/"
mkdir -p "./.cache/fr/departements/france_geojson/"
mkdir -p "./.cache/fr/regions/geo_api_gouv_fr/"

echo -n "" > geo-data-departements-fr.js

for file in ./data/fr/departements/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	code=$(basename "$file" .yml)
	description=$(cat $file | yq -r .fr.description)
	description=$(sed 's/"/\\"/g' <<<$description)
	url=$(cat $file | yq -r .fr.url)
	if [ ! -f "./.cache/fr/departements/geo_api_gouv_fr/${code}.txt" ]; then
		cache_departement=$(curl --silent "https://geo.api.gouv.fr/departements?code=${code}")
		echo "${cache_departement}" > "./.cache/fr/departements/geo_api_gouv_fr/${code}.txt"
	else
		cache_departement=$(cat "./.cache/fr/departements/geo_api_gouv_fr/${code}.txt")
	fi
	codeRegion=$(echo ${cache_departement} | jq -r .[0].codeRegion)
	nom=$(echo ${cache_departement} | jq -r .[0].nom)

	if [ ! -f "./.cache/fr/regions/geo_api_gouv_fr/${codeRegion}.txt" ]; then
		cache_region=$(curl --silent "https://geo.api.gouv.fr/regions?code=${codeRegion}")
		echo "${cache_region}" > "./.cache/fr/regions/geo_api_gouv_fr/${codeRegion}.txt"
	else
		cache_region=$(cat "./.cache/fr/regions/geo_api_gouv_fr/${codeRegion}.txt")
	fi
	nomRegion=$(echo ${cache_region} | jq .[0].nom | sed 's/\"//g' | awk '{print tolower($0)}' | tr "' " "--")
	nomRegion=$(echo ${nomRegion} | iconv -f utf8 -t ascii//TRANSLIT)

	if [ ! -f "./.cache/fr/departements/france_geojson/${code}.txt" ]; then
		cache_geo=$(curl --silent "https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/regions/${nomRegion}/departements-${nomRegion}.geojson" | jq -c "{features: [ .features[]| select( .properties.code == \"$code\") ] } | .features[0].geometry")
		echo "${cache_geo}" > "./.cache/fr/departements/france_geojson/${code}.txt"
	else
		cache_geo=$(cat "./.cache/fr/departements/france_geojson/${code}.txt")
	fi

	categories=""
	for type in $(cat $file | yq -c -r '.meta.types[]'); do
		types="${types} ${type}"
		types_list[$type]="departement_fr_${code},${types_list[$type]}"
		if [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta') != 'null' ]] && [[ $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]') != 'null' ]]; then
			for depends_on in $(cat ./data/meta/types/${type}.yml | yq -c -r '.meta.depends_on[]'); do
				types="${types} ${depends_on}"
				types_list[$depends_on]="departement_fr_${code},${types_list[$depends_on]}"
			done
		fi
		categories="\"${type}\",${categories}"
	done

	if [ "$url" == "null" ]; then
		echo "var departement_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\", \"categories\": [${categories}]},\"geometry\":$cache_geo}, departementsProperties);" >> geo-data-departements-fr.js
	else
		echo "var departement_fr_${code} = L.geoJSON({\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\", \"categories\": [${categories}],\"url\":\"$url\"},\"geometry\":$cache_geo}, departementsProperties);" >> geo-data-departements-fr.js
	fi
	ids="departement_fr_${code},${ids}"
done

echo "var typeDepartementsFr = {" >> geo-data-departements-fr.js
for type in $(echo $types | xargs -n1 | sort -u | xargs); do
	echo "  ${type}: [${types_list[$type]}]," >> geo-data-departements-fr.js
done
echo "};" >> geo-data-departements-fr.js

echo "var geoDataDepartementsFr = [${ids}];" >> geo-data-departements-fr.js
