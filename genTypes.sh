#!/usr/bin/env bash

set -e

echo -n "" > ./meta-types-filter.js

for file in ./data/meta/types/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	id=$(basename "$file" .yml)
	echo "var layerGroup_${id} = L.layerGroup((typeCitiesFr.${id}||[]).concat(typeDepartementsFr.${id}||[]).concat(typeRegionsFr.${id}||[]).concat(typeCitiesBe.${id}||[]));" >> ./meta-types-filter.js
done

echo "" >> ./meta-types-filter.js
echo "var layersTitles = {" >> ./meta-types-filter.js

for file in ./data/meta/types/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	id=$(basename "$file" .yml)
	title=$(cat $file | yq -r .fr.title)
	title=$(sed 's/"/\\"/g' <<<$title)
	echo "  \"${title}\": layerGroup_${id}," >> ./meta-types-filter.js
done

echo "};" >> ./meta-types-filter.js
echo "" >> ./meta-types-filter.js
echo -n "var layers = [" >> ./meta-types-filter.js

for file in ./data/meta/types/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	id=$(basename "$file" .yml)
	title=$(cat $file | yq -r .fr.title)
	title=$(sed 's/"/\\"/g' <<<$title)
	echo -n "layerGroup_${id}.addTo(map)," >> ./meta-types-filter.js
done

echo "];" >> ./meta-types-filter.js

echo "var filtersI18n = {" > ./meta-i18n-filter.js

for file in ./data/meta/types/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	id=$(basename "$file" .yml)
	title=$(cat $file | yq -r .fr.title)
	title=$(sed 's/"/\\"/g' <<<$title)
	echo "  ${id}: \"${title}\"," >> ./meta-i18n-filter.js
done

echo "};" >> ./meta-i18n-filter.js
