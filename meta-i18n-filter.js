var filtersI18n = {
  cameras_parlantes: "Caméras parlantes",
  cameras_thermiques: "Caméras thermiques",
  capteurs_sonores: "Capteurs sonores",
  divers: "Divers",
  drones: "Drones",
  police_predictive: "Police prédictive",
  rf: "Reconnaissance faciale",
  vsa: "Vidéosurveillance automatisée",
};
