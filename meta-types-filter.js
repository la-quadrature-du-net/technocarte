var layerGroup_cameras_parlantes = L.layerGroup((typeCitiesFr.cameras_parlantes||[]).concat(typeDepartementsFr.cameras_parlantes||[]).concat(typeRegionsFr.cameras_parlantes||[]).concat(typeCitiesBe.cameras_parlantes||[]));
var layerGroup_cameras_thermiques = L.layerGroup((typeCitiesFr.cameras_thermiques||[]).concat(typeDepartementsFr.cameras_thermiques||[]).concat(typeRegionsFr.cameras_thermiques||[]).concat(typeCitiesBe.cameras_thermiques||[]));
var layerGroup_capteurs_sonores = L.layerGroup((typeCitiesFr.capteurs_sonores||[]).concat(typeDepartementsFr.capteurs_sonores||[]).concat(typeRegionsFr.capteurs_sonores||[]).concat(typeCitiesBe.capteurs_sonores||[]));
var layerGroup_divers = L.layerGroup((typeCitiesFr.divers||[]).concat(typeDepartementsFr.divers||[]).concat(typeRegionsFr.divers||[]).concat(typeCitiesBe.divers||[]));
var layerGroup_drones = L.layerGroup((typeCitiesFr.drones||[]).concat(typeDepartementsFr.drones||[]).concat(typeRegionsFr.drones||[]).concat(typeCitiesBe.drones||[]));
var layerGroup_police_predictive = L.layerGroup((typeCitiesFr.police_predictive||[]).concat(typeDepartementsFr.police_predictive||[]).concat(typeRegionsFr.police_predictive||[]).concat(typeCitiesBe.police_predictive||[]));
var layerGroup_rf = L.layerGroup((typeCitiesFr.rf||[]).concat(typeDepartementsFr.rf||[]).concat(typeRegionsFr.rf||[]).concat(typeCitiesBe.rf||[]));
var layerGroup_vsa = L.layerGroup((typeCitiesFr.vsa||[]).concat(typeDepartementsFr.vsa||[]).concat(typeRegionsFr.vsa||[]).concat(typeCitiesBe.vsa||[]));

var layersTitles = {
  "Caméras parlantes": layerGroup_cameras_parlantes,
  "Caméras thermiques": layerGroup_cameras_thermiques,
  "Capteurs sonores": layerGroup_capteurs_sonores,
  "Divers": layerGroup_divers,
  "Drones": layerGroup_drones,
  "Police prédictive": layerGroup_police_predictive,
  "Reconnaissance faciale": layerGroup_rf,
  "Vidéosurveillance automatisée": layerGroup_vsa,
};

var layers = [layerGroup_cameras_parlantes.addTo(map),layerGroup_cameras_thermiques.addTo(map),layerGroup_capteurs_sonores.addTo(map),layerGroup_divers.addTo(map),layerGroup_drones.addTo(map),layerGroup_police_predictive.addTo(map),layerGroup_rf.addTo(map),layerGroup_vsa.addTo(map),];
